package library;

import library.service.Author;
import library.service.Book;
import library.service.Service;
import library.service.ServiceImpl;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

@Component
public class ExampleOfServiceWork {

    @Autowired
    Service service;

    @PostConstruct
    public void testIt() {

        System.out.println("FILLING DB");

        Author martin = new Author("George Martin", 1948, new DateTime());
        Author twen = new Author("Mark Twen", 1835, new DateTime("1940-04-21"));
        Author king = new Author("Stiven King", 1947, new DateTime());
        Author pelevin = new Author("Voktor Pelevin", 1962, new DateTime());

        service.addAuthor(martin);
        service.addAuthor(twen);
        service.addAuthor(king);
        service.addAuthor(pelevin);

        service.addBookByAuthor(martin, new Book(1000, "Game of thrones", new DateTime("1996-04-06")));
        service.addBookByAuthor(twen, new Book(2000, "Tom Soyer", new DateTime("1876-02-01")));
        service.addBookByAuthor(king, new Book(1000, "Green mile", new DateTime("1996-04-01")));
        service.addBookByAuthor(king, new Book(900, "Shine", new DateTime("1977-01-21")));
        service.addBookByAuthor(pelevin, new Book(2200, "Generation Pi", new DateTime("1999-01-12")));
        service.addBookByAuthor(pelevin, new Book(2300, "Omon Ra", new DateTime("1992-10-01")));

        System.out.println("FILLING COMPLETE");
        System.out.println();

        System.out.println("SHOW BD:");
        service.showBD();

        System.out.println("LOAD BD IN FILE");
        service.saveAllBooksInFile("C:\\RESULT\\mart.txt");
        System.out.println("LOAD COMPLETE");
        System.out.println();
        System.out.println("LOAD BD IN XML");
        service.saveAllBooksInXml("C:\\RESULT\\mart.xml");
        System.out.println("LOAD COMPLETE");

        System.out.println();

        System.out.println("SHOW ALL BOOKS BY AUTHOR WHO OLDER THEN 63:");
        for (Book book : service.getAllBooksByAuthorsWhoOlderThen(63)) {
            System.out.println(book.toString());
        }
        System.out.println();

        System.out.println("SHOW ALL BOOKS BY AUTHOR WHO YOUNGER THEN 63:");
        for (Book book : service.getAllBooksByAuthorsWhoYoungerThen(63)) {
            System.out.println(book.toString());
        }
        System.out.println();

        System.out.println("DELETE ALL BOOKS BY GEORGE MARTIN");
        service.deleteAuthor(martin);
        System.out.println("DELETE ALL BOOKS BY GEORGE MARTIN COMPLETE");
        System.out.println();

        System.out.println("SHOW BD");
        service.showBD();

        service.clean();
    }
}
