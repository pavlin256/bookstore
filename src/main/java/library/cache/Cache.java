package library.cache;

import library.service.Author;
import library.service.Book;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class Cache {
    private Map<String, List<Author>> authors = new HashMap<String, List<Author>>();
    private Map<String, List<Book>> books = new HashMap<String, List<Book>>();

    public ArrayList<Author> getAuthorsCache(String key) {
        return (ArrayList<Author>) authors.get(key);
    }

    public ArrayList<Book> getBooksCache(String key) {
        return (ArrayList<Book>) books.get(key);
    }

    public void addToBookCache(String methodName, List<Book> result) {
        books.put(methodName, result);
    }

    public void addToAuthorsCache(String methodName, List<Author> result) {
        authors.put(methodName, result);
    }

    public void clear() {
        books.clear();
        authors.clear();
    }
}
