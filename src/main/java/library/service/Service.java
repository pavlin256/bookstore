package library.service;

import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public interface Service {

    void showBD();

    void showAllBooksByAuthors(ArrayList<Author> authors);

    void importFromXml(String path);

    ArrayList<Book> getAllBooks();
    ArrayList<Book> getAllBooksByAuthor(Author author);
    ArrayList<Book> getAllBooksByAuthorsWhoOlderThen(int age);
    ArrayList<Book> getAllBooksByAuthorsWhoYoungerThen(int age);

    void addAuthor(Author author);
    void addBookByAuthor(Author author, Book book);
    void deleteAllBooksByAuthor(Author author);
    void deleteAuthor(Author author);
    void deleteBook(Book book);

    Author getAuthorByName(String s);

    void saveAllBooksInFile(String sad);
    void saveAllBooksInXml(String sad);
    void clean();
}
