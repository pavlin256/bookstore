package library.service;

import org.joda.time.DateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "book")
@XmlAccessorType(XmlAccessType.FIELD)
public class Book {

    private int id;
    private int price;
    private String name;
    private int authorId;
    private DateTime releaseDate;

    @Deprecated
    public Book(){}

    public Book(int price, String name, DateTime releaseDate) {
        this.price = price;
        this.name = name;
        this.releaseDate = releaseDate;
    }

    public Book(int id, int price, String name, int authorId, DateTime releaseDate) {
        this.id = id;
        this.price = price;
        this.name = name;
        this.authorId = authorId;
        this.releaseDate = releaseDate;
    }

    public void setAuthorId(int id){
        this.authorId = id;
    }

    public int getId() {
        return id;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public int getAuthorId() {
        return authorId;
    }

    public DateTime getReleaseDate() {
        return releaseDate;
    }

    @Override
    public String toString() {
        return "BOOK" + " : " + this.getName() + "    "
            + "PRICE " + " : " + this.getPrice() +  "   "
                + "RELEASE DATE" +  "  :  " + this.getReleaseDate().getYear() + "." + this.getReleaseDate().getMonthOfYear() + "." + this.getReleaseDate().getDayOfMonth()
                    ;
    }
}
