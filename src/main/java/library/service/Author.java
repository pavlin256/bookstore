package library.service;

import org.joda.time.DateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "author")
@XmlAccessorType(XmlAccessType.FIELD)
public class Author {
    private int id;
    private String name;
    private int birthYear;
    private DateTime dateOfDeath;

    @XmlElement(name = "book")
    ArrayList<Book> books;


    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public Author() {}

    public Author(String name, int birthYear, DateTime dateOfDeath) {
        this.name = name;
        this.birthYear = birthYear;
        this.dateOfDeath = dateOfDeath;
    }

    public Author(int id, String name, int birthYear, DateTime dateTime) {
        this.id = id;
        this.name = name;
        this.birthYear = birthYear;
        this.dateOfDeath = dateTime;
    }


    public void setDateOfDeath(DateTime dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public DateTime getDateOfDeath() {
        return dateOfDeath;
    }

    @Override
    public String toString() {
        return this.getName() + " " + this.getBirthYear() + " " + this.getDateOfDeath();
    }


}
