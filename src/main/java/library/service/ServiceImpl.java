package library.service;

import library.io.FileManager;
import library.cache.Cache;
import library.dao.AuthorDao;
import library.dao.BookDao;
import library.io.XmlManager;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Component
@Transactional
public class ServiceImpl implements Service {

    @Autowired
    private BookDao bookDao;
    @Autowired
    private AuthorDao authorDao;
    @Autowired
    private XmlManager xmlManager;
    @Autowired
    private FileManager fileManager;
    @Autowired
    private Cache cache;


    @Override
    public void addAuthor(Author author) {
        authorDao.addAuthor(author);
        cache.clear();
    }

    @Override
    public void addBookByAuthor(Author author, Book book) {
        ArrayList<Author> authors = getAllAuthors();
        for (Author oldAuthor : authors) {
            if (oldAuthor.getName().equals(author.getName())) {
                bookDao.addBook(book, oldAuthor);
                cache.clear();
                return;
            }
        }
        authorDao.addAuthor(author);
        bookDao.addBook(book, author);
        cache.clear();
    }


    @Override
    public ArrayList<Book> getAllBooks() {
        ArrayList<Book> result = cache.getBooksCache("getAllBooks");
        if (result == null) {
            result = bookDao.getAllBooks();
            cache.addToBookCache("getAllBooks", result);
        }
        return result;
    }

    @Override
    public void deleteBook(Book book) {
        bookDao.deleteBook(book);
        cache.clear();
    }

    @Override
    public ArrayList<Book> getAllBooksByAuthor(Author author) {
        ArrayList<Book> result = cache.getBooksCache(author.getName());
        if (result == null) {
            result = bookDao.getAllBooksByAutor(author);
            cache.addToBookCache(author.getName(), result);
        }
        return result;
    }

    @Override
    public ArrayList<Book> getAllBooksByAuthorsWhoOlderThen(int age) {
        ArrayList<Book> books = cache.getBooksCache("getAllBooksByAuthorsWhoOlderThen" + age);
        if (books == null) {
            books = new ArrayList<Book>();
            ArrayList<Author> authors = getAllAuthors();
            DateTime now = new DateTime();
            for (Author author : authors) {
                if (now.getYear() - author.getBirthYear() > age) {
                    books.addAll(author.books);
                }
            }
            cache.addToBookCache("getAllBooksByAuthorsWhoOlderThen" + age, books);
        }
        return books;
    }
    @Override
    public ArrayList<Book> getAllBooksByAuthorsWhoYoungerThen(int age) {
        ArrayList<Book> books = cache.getBooksCache("getAllBooksByAuthorsWhoYoungerThen"+age);
        if (books == null) {
            books = new ArrayList<Book>();
            ArrayList<Author> authors = getAllAuthors();
            DateTime now = new DateTime();
            for (Author author : authors) {
                if (now.getYear() - author.getBirthYear() < age) {
                    books.addAll(author.books);
                }
            }
        }
        cache.addToBookCache("getAllBooksByAuthorsWhoYoungerThen" + age, books);
        return books;
    }


    @Override
    public void deleteAllBooksByAuthor(Author author) {
        ArrayList<Book> books = getAllBooksByAuthor(author);
        for (Book book : books) {
            bookDao.deleteBook(book);
        }
        cache.clear();
    }

    @Override
    public void deleteAuthor(Author author) {
        deleteAllBooksByAuthor(author);
        authorDao.deleteAuthor(author);
        cache.clear();
    }

    public Author getAuthorByName(String s) {
        ArrayList<Author> authors = getAllAuthors();
        for (Author author : authors) {
            if (author.getName().equals(s)) return author;
        }
        throw new RuntimeException("NO AUTHOR WITH SUCH NAME");
    }

    @Override
    public void saveAllBooksInFile(String fileName) {
        ArrayList<Author> authors = getAllAuthors();
        fileManager.saveTo(fileName, authors);
    }


    @Override
    public void saveAllBooksInXml(String fileName) {
        ArrayList<Author> authors = getAllAuthors();
        xmlManager.saveTo(fileName, authors);
    }

    @Override
    public void showBD() {
        ArrayList<Author> authors = getAllAuthors();
        showAllBooksByAuthors(authors);
    }

    @Override
    public void showAllBooksByAuthors(ArrayList<Author> authors){
        for (Author author : authors) {
            System.out.println(author.getName() + " was born at " + author.getBirthYear());
            for (Book book : author.books){
                System.out.println("    " + book.getName() + ";   cost " + book.getPrice() + ";  released at " + book.getReleaseDate().getYear()+"."+book.getReleaseDate().getMonthOfYear()+"."+book.getReleaseDate().getDayOfMonth());
            }
            System.out.println();
        }
        System.out.println();
    }

    @Override
    public void importFromXml(String path) {
        bookDao.clean();
        authorDao.clean();
        cache.clear();

        ArrayList<Author> authors = xmlManager.importFrom(path);
        for (Author author : authors) {

            for (Book book : author.getBooks())
                addBookByAuthor(author, book);
        }
    }

    public ArrayList<Author> getAllAuthors() {
        ArrayList<Author> authors = cache.getAuthorsCache("getAllAuthors");
        if (authors == null) {
            authors = authorDao.getAllAuthors();
            cache.addToAuthorsCache("getAllAuthors", authors);
        }

        for (Author author : authors) {
            ArrayList<Book> books = getAllBooksByAuthor(author);
            author.setBooks(books);
        }

        return authors;
    }


    public void clean() {
        authorDao.clean();
        bookDao.clean();
        cache.clear();
    }
}