package library.io;

import library.service.Author;
import library.service.Book;

import java.util.ArrayList;

public interface Saver {
    void saveTo(String path, ArrayList<Author> authors);
}
