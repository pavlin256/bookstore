package library.io;

import library.service.Author;

import java.util.ArrayList;

public interface Importer {
    ArrayList<Author> importFrom(String path);
}
