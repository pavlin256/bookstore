package library.io;

import library.service.Author;
import library.service.Book;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


@Component
public class FileManager implements Saver {
    @Override
    public void saveTo(String fileName, ArrayList<Author> authors) {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
            for (Author author : authors) {
                out.write(author.getName() + " was born at " + author.getBirthYear() + " year.");
                out.newLine();
                for (Book book : author.getBooks()) {
                    out.write("   " + book.toString() + "\n");
                    out.newLine();
                }
                out.newLine();
            }
            out.close();
        } catch (IOException e) {
            throw new RuntimeException("can't write to file " + fileName);
        }
    }
}
