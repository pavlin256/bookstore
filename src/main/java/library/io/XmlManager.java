package library.io;

import library.service.Author;
import library.service.BookStore;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;

@Component
public class XmlManager implements Saver, Importer {
    @Override
    public ArrayList<Author> importFrom(String path) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(BookStore.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            BookStore libraryNEW = (BookStore) jaxbUnmarshaller.unmarshal( new File(path) );
            return libraryNEW.getAuthors();

        } catch (JAXBException e) {
            throw  new RuntimeException("Can't open xml from " + path);
        }
    }

    @Override
    public void saveTo(String path, ArrayList<Author> authors) {
        BookStore library = new BookStore();
        library.setAuthors(authors);
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(BookStore.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(library, new File(path));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

}
