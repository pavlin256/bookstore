package library.dao;

import library.service.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class AuthorDao {

    private int MAX_DB_SIZE = 100000;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void addAuthor(Author newAuthor) {
        jdbcTemplate.update(AuthorRequest.INSERT.toString(), getFreeId(), newAuthor.getName(), newAuthor.getBirthYear(), newAuthor.getDateOfDeath().toDate());
    }

    public void showAllAuthors() {
        final List query = jdbcTemplate.query(AuthorRequest.SELECT.toString(), new AuthorMapper());
        for (Object author : query) {
            Author author1 = (Author)author;
            System.out.println(author1.getId() + " " + author1.getName() + " " + author1.getBirthYear() + " " + author1.getDateOfDeath());
        }
    }


    public void deleteAuthor(Author author) {
        ArrayList<Author> authors = getAllAuthors();
        for (Author checkAuthor : authors) {
            if (checkAuthor.getName().equals(author.getName())) {
                jdbcTemplate.update(AuthorRequest.DELETE.toString(), checkAuthor.getId());
            }
        }
    }

    public ArrayList<Author> getAllAuthors() {
        ArrayList<Author> authors = new ArrayList<Author>();
        final List query = jdbcTemplate.query(AuthorRequest.SELECT.toString(), new AuthorMapper());
        for (Object author : query) {
            authors.add((Author) author);
        }
        return authors;
    }

    public void clean() {
        jdbcTemplate.update(AuthorRequest.DELETE_All.toString());
    }

    public int getFreeId() {
        int[] id = new int[MAX_DB_SIZE];
        final List query = jdbcTemplate.query(AuthorRequest.SELECT.toString(), new AuthorMapper());
        for (Object author : query) {
            Author author1 = (Author)author;
            id[author1.getId()] = 1;
        }
        for (int i = 1; i < id.length; i++) {
            if (id[i] == 0) return i;
        }
        return id.length;
    }
}
