package library.dao;

import library.service.Author;
import library.service.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class BookDao {

    private int MAX_DB_SIZE = 100000;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void showAllBooks() {
        final List query = jdbcTemplate.query(BookRequest.SELECT.toString(), new BookMapper());
        for (Object book : query) {
            System.out.println(((Book) book).getName() + " " +  ((Book) book).getPrice());
        }
    }

    public void addBook(Book newBook, Author author) {
        Date d = newBook.getReleaseDate().toDate();
        jdbcTemplate.update(BookRequest.INSERT.toString(), getFreeId(), newBook.getPrice(), newBook.getName(), author.getId(), d);
    }


    public ArrayList<Book> getAllBooks() {
        ArrayList<Book> allBooks = new ArrayList<Book>();
        final List query = jdbcTemplate.query(BookRequest.SELECT.toString(), new BookMapper());
        for (Object book : query) {
            allBooks.add((Book) book);
        }
        return allBooks;
    }


    public void deleteBook(Book book) {
        ArrayList<Book> books = getAllBooks();
        for (Book oldBook : books){
            if (oldBook.getName().equals(book.getName())) {
                jdbcTemplate.update(BookRequest.DELETE.toString(), oldBook.getId());
            }
        }
    }

    public ArrayList<Book> getAllBooksByAutor(Author author) {
        ArrayList<Book> allBooks = getAllBooks();
        ArrayList<Book> allBooksByAuthor = new ArrayList<Book>();
        for (Book book : allBooks) {
            book.getAuthorId();
            if (book.getAuthorId() == author.getId()) {
                allBooksByAuthor.add(book);
            }
        }
        return allBooksByAuthor;
    }

    public int getFreeId() {
        int[] id = new int[MAX_DB_SIZE];
        final List query = jdbcTemplate.query(BookRequest.SELECT.toString(), new BookMapper());
        for (Object book : query) {
            Book book1 = (Book)book;
            id[book1.getId()] = 1;
        }
        for (int i = 1; i < id.length; i++) {
            if (id[i] == 0) return i;
        }
        return id.length;
    }

    public void clean() {
        jdbcTemplate.update(BookRequest.DELETE_ALL.toString());
    }

}
