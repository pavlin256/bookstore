package library.dao;

/**
 * Created by Student on 20.04.2015.
 */
public enum BookRequest {
    SELECT("SELECT * FROM books"),
    DELETE("DELETE FROM books WHERE id = ?"),
    DELETE_ALL("DELETE FROM books"),
    INSERT("INSERT INTO books VALUES (?, ?, ?, ?, ?)");

    public String s;

    BookRequest(String s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return this.s;
    }
}
