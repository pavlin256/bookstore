package library.dao;

import library.service.Author;
import org.joda.time.DateTime;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthorMapper implements org.springframework.jdbc.core.RowMapper {
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        int id = resultSet.getInt(1);
        String name = resultSet.getString(2);
        int birthYear = resultSet.getInt(3);
        DateTime dateTime = new DateTime(resultSet.getDate(4));
        return new Author(id, name, birthYear, dateTime);
    }
}
