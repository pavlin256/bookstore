package library.dao;

import library.service.Book;
import org.joda.time.DateTime;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Student on 20.04.2015.
 */
public class BookMapper implements org.springframework.jdbc.core.RowMapper {
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        int id = resultSet.getInt(1);
        String name = resultSet.getString(3);
        int price = resultSet.getInt(2);
        int authorId = resultSet.getInt(4);
        DateTime dateTime = new DateTime(resultSet.getDate(5));
        return new Book(id, price, name, authorId, dateTime);
    }
}
