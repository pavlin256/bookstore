package library.dao;

public enum AuthorRequest {
    SELECT("SELECT * FROM AUTHORS"),
    SELECT_BY_NAME("SELECT * FROM AUTHORS WHERE name = ?"),
    DELETE("DELETE FROM AUTHORS WHERE id = ?"),
    DELETE_All("DELETE FROM AUTHORS"),
    INSERT("INSERT INTO AUTHORS VALUES (?, ?, ?, ?)");

    public String s;

    AuthorRequest(String s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return this.s;
    }
}
