package library.service;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.mock;

public class AuthorTest {

    private DateTime deathDate;
    private Author author;
    private ArrayList<Book> books;

    @Before
    public void setUp() {
        Book newBook = new Book(100, "a", new DateTime());
        books = new ArrayList<Book>();
        books.add(newBook);
        deathDate =  new DateTime();
        author = new Author(1, "Stiven King", 100, deathDate);
        author.setBooks(books);
    }

    @Test
    public void testGetBooks() throws Exception {
        Assert.assertArrayEquals(books.toArray(), author.getBooks().toArray());
    }

    @Test
    public void testGetId() throws Exception {
        Assert.assertEquals(1, author.getId());
    }

    @Test
    public void testGetName() throws Exception {
        Assert.assertEquals("Stiven King", author.getName());
    }

    @Test
    public void testGetBirthYear() throws Exception {
        Assert.assertEquals(100, author.getBirthYear());
    }

    @Test
    public void testGetDateOfDeath() throws Exception {
        Assert.assertEquals(deathDate, author.getDateOfDeath());
    }
}