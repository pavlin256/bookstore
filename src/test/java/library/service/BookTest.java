package library.service;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BookTest {
    private DateTime releaseDate;
    private Book book;

    @Before
    public void setUp() throws Exception {
        releaseDate = new DateTime();
        book = new Book(1, 1000, "Green Mile", 1, releaseDate);
    }

    @Test
    public void testGetId() throws Exception {
        Assert.assertEquals(1, book.getId());
    }

    @Test
    public void testGetPrice() throws Exception {
        Assert.assertEquals(1000, book.getPrice());
    }

    @Test
    public void testGetName() throws Exception {
        Assert.assertEquals("Green Mile", book.getName());
    }

    @Test
    public void testGetAuthorId() throws Exception {
        Assert.assertEquals(1, book.getId());
    }
}