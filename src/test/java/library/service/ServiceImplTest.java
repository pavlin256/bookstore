package library.service;

import library.cache.Cache;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ServiceImplTest {

    private Service service = new ServiceImpl();
    private Cache cache;
    private Book book1;
    private Book book2;
    private Author martin;
    private Author king;

    @Before
    public void setUp() throws Exception {

        martin = new Author("George Martin", 1948, new DateTime());
        king = new Author("Stiven King", 1927, new DateTime());

        book1 = new Book(1000, "Game of thrones", new DateTime("1996-04-06"));
        book2 = new Book(1000, "Green Mile", new DateTime("1996-04-01"));

        ArrayList<Book> books1 = new ArrayList<Book>();
        books1.add(book1);
        ArrayList<Book> books2 = new ArrayList<Book>();
        books2.add(book2);
        ArrayList<Book> books3 = new ArrayList<Book>();
        books3.add(book1); books3.add(book2);

        martin.setBooks(books1);
        king.setBooks(books2);

        cache = mock(Cache.class);

        when(cache.getBooksCache("getAllBooks")).thenReturn(books3);
        when(cache.getBooksCache("George Martin")).thenReturn(books1);
        when(cache.getBooksCache("Stiven King")).thenReturn(books2);
        when(cache.getBooksCache("getAllBooksByAuthorsWhoOlderThen80")).thenReturn(books2);
        when(cache.getBooksCache("getAllBooksByAuthorsWhoYoungerThen60")).thenReturn(books1);
    }


    @Test
    public void testGetAllBooks() throws Exception {

        Assert.assertArrayEquals(Arrays.asList(book1, book2).toArray(), service.getAllBooks().toArray());
    }

    @Test
    public void testGetAllBooksByAuthor() throws Exception {
        Assert.assertArrayEquals(Collections.singletonList(book1).toArray(), service.getAllBooksByAuthor(martin).toArray());
        Assert.assertArrayEquals(Collections.singletonList(book2).toArray(), service.getAllBooksByAuthor(king).toArray());
    }

    @Test
    public void testGetAllBooksByAuthorsWhoOlderThen() throws Exception {
        Assert.assertArrayEquals(Collections.singletonList(book2).toArray(), service.getAllBooksByAuthorsWhoOlderThen(80).toArray());
    }

    @Test
    public void testGetAllBooksByAuthorsWhoYoungerThen() throws Exception {
        Assert.assertArrayEquals(Collections.singletonList(book2).toArray(), service.getAllBooksByAuthorsWhoYoungerThen(60).toArray());
    }

}